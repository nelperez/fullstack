from django.shortcuts import render
from .models import Student, Teacher, Course, StudentCourse, Quiz, QuizCourseStudent

# Create your views here.

def home(request):
	data = {}
	data['view_title'] = 'home'
	return render(request, "university/home.html", data)


def teacher(request):
	data = {}
	data['view_title'] = 'Profesores'
	return render(request, "university/teacher.html", data)	


def student(request):
	data = {}
	data['view_title'] = 'Estudiantes'
	return render(request, "university/student.html", data)


def course(request):
	data = {}
	data['view_title'] = 'Cursos'
	return render(request, "university/course.html", data)	


def quiz(request):
	data = {}
	data['view_title'] = 'Pruebas'
	return render(request, "university/quiz.html", data)


def score(request):
	data = {}
	data['view_title'] = 'Notas'
	return render(request, "university/score.html", data)		

def calendar(request):
	data = {}
	data['view_title'] = 'Calendario'
	return render(request, "university/calendar.html", data)		