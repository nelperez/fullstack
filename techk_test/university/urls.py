from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.home, name="home"),
    url(r'^teachers/$', views.teacher, name="teachers"),
    url(r'^students/$', views.student, name="students"),
	url(r'^course/$', views.course, name="courses"),
	url(r'^quiz/$', views.quiz, name="quizzes"),
	url(r'^score/$', views.score, name="scores"),
	url(r'^calendar/$', views.calendar, name="calendar"),
]
