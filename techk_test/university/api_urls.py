# -*- coding: utf-8 -*-
from django.conf.urls import url
from django.conf.urls import include
from. import api

urlpatterns = [
    url(r'^teacher/$', api.TeacherView.as_view(), name='teachers_list_and_create'),
    url(r'^teacher/(?P<pk>[0-9]+)/$', api.TeacherView.as_view(), name='teachers_delete_and_edit'),
    url(r'^student/$', api.StudentView.as_view(), name='students_list_and_create'),
    url(r'^student/(?P<pk>[0-9]+)/$', api.StudentView.as_view(), name='students_delete_and_edit'),
    url(r'^course/$', api.CourseView.as_view(), name='courses_list_and_create'),
    url(r'^course/(?P<pk>[0-9]+)/$', api.CourseView.as_view(), name='courses_delete_and_edit'),
    url(r'^quiz/$', api.QuizView.as_view(), name='quizzes_list_and_create'),
    url(r'^quiz/(?P<pk>[0-9]+)/$', api.QuizView.as_view(), name='quizzes_delete_and_edit'),
    url(r'^student_course/$', api.StudentCourseView.as_view(), name='student_course_create'),
    url(r'^student_course/(?P<student_pk>[0-9]+)/(?P<course_pk>[0-9]+)/$', api.StudentCourseView.as_view(), name='student_course_delete'),
    url(r'^score/$', api.ScoreView.as_view(), name='scores_list_and_create'),
    url(r'^score/(?P<pk>[0-9]+)/$', api.ScoreView.as_view(), name='scores_delete_and_edit'),
    url(r'^student/avg/$', api.StudentsAvg.as_view(), name='students_list_avg')
]
