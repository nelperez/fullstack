# -*- coding: utf-8 -*-
from rest_framework.views import APIView
from rest_framework import status
from django.core.serializers import serialize
from django.http import HttpResponse, JsonResponse
from django.forms.models import model_to_dict
from.models import Teacher
from.models import Student
from.models import Course
from.models import Quiz
from.models import StudentCourse
from.models import QuizCourseStudent
from django.db.models import Avg
import json


class TeacherView(APIView):

	def get(self, request):
		teachers = serialize("json", Teacher.objects.all())
		return HttpResponse(teachers, content_type="json", status=status.HTTP_200_OK)

	def delete(self, request, pk):
		try:
			teacher = Teacher.objects.get(id=pk)
			teacher.delete()
			return JsonResponse({"status":"ok"}, status=status.HTTP_200_OK, safe=False)
		except Teacher.DoesNotExist:
			return JsonResponse({"status":"error not found"}, status=status.HTTP_404_NOT_FOUND \
				, safe=False)

	def post(self, request):
		try:
			teacher = Teacher.objects.create(
				name=request.data.get('name', None),
				last_name_1=request.data.get('last_name_1', None),
				last_name_2=request.data.get('last_name_2', None)
			)
			return JsonResponse(json.loads(serialize('json', [teacher,]))[0], \
				status=status.HTTP_200_OK, safe=False)
		except:
			return JsonResponse({"status":"error"}, status=status.HTTP_400_BAD_REQUEST, safe=False)

	def put(self, request, pk):
		try:
			teacher = Teacher.objects.get(id=pk)
		except Teacher.DoesNotExist:
			return HttpResponse({"status":"error not found"}, content_type="json", \
				status=status.HTTP_404_NOT_FOUND)

		try:	
			teacher.name = request.data.get('name', None)
			teacher.last_name_1 = request.data.get('last_name_1', None)
			teacher.last_name_2 = request.data.get('last_name_2', None)
			teacher.save()
			return JsonResponse(json.loads(serialize('json', [teacher,]))[0], \
				status=status.HTTP_200_OK, safe=False)
		except:
			return JsonResponse({"status":"error"}, status=status.HTTP_400_BAD_REQUEST, safe=False)


class StudentView(APIView):

	def get(self, request):
		operation = request.GET.get('operation', None)
		course = request.GET.get('course', None)
		if operation and course:
			if operation == 'course_in':
				students = Student.objects.filter(id__in=list(StudentCourse.objects.filter(course=course).values_list('student', flat=True)))
			elif operation == 'course_out':
				students = Student.objects.exclude(id__in=list(StudentCourse.objects.filter(course=course).values_list('student', flat=True)))
			return HttpResponse(serialize("json", students), content_type="json", status=status.HTTP_200_OK)
		else:		
			return HttpResponse(serialize("json", Student.objects.all()), content_type="json", status=status.HTTP_200_OK)

	def delete(self, request, pk):
		try:
			student = Student.objects.get(id=pk)
			student.delete()
			return JsonResponse({"status":"ok"}, status=status.HTTP_200_OK, safe=False)
		except Student.DoesNotExist:
			return JsonResponse({"status":"error not found"}, status=status.HTTP_404_NOT_FOUND \
				, safe=False)

	def post(self, request):
		try:
			student = Student.objects.create(
				name=request.data.get('name', None),
				last_name_1=request.data.get('last_name_1', None),
				last_name_2=request.data.get('last_name_2', None),
				identity_card_number=request.data.get('identity_card_number', None)
			)
			return JsonResponse(json.loads(serialize('json', [student,]))[0], \
				status=status.HTTP_200_OK, safe=False)
		except:
			return JsonResponse({"status":"error"}, status=status.HTTP_400_BAD_REQUEST, safe=False)

	def put(self, request, pk):
		try:
			student = Student.objects.get(id=pk)
		except Student.DoesNotExist:
			return HttpResponse({"status":"error not found"}, content_type="json", \
				status=status.HTTP_404_NOT_FOUND)

		try:	
			student.name = request.data.get('name', None)
			student.last_name_1 = request.data.get('last_name_1', None)
			student.last_name_2 = request.data.get('last_name_2', None)
			student.identity_card_number=request.data.get('identity_card_number', None)
			student.save()
			return JsonResponse(json.loads(serialize('json', [student,]))[0], \
				status=status.HTTP_200_OK, safe=False)
		except:
			return JsonResponse({"status":"error"}, status=status.HTTP_400_BAD_REQUEST, safe=False)


class CourseView(APIView):

	def get(self, request):
		courses = self.toDictList(Course.objects.all().select_related('teacher'))
		return HttpResponse(json.dumps(courses), content_type="json", status=status.HTTP_200_OK)

	def delete(self, request, pk):
		try:
			course = Course.objects.get(id=pk)
			course.delete()
			return JsonResponse({"status":"ok"}, status=status.HTTP_200_OK, safe=False)
		except Course.DoesNotExist:
			return JsonResponse({"status":"error not found"}, status=status.HTTP_404_NOT_FOUND \
				, safe=False)

	def post(self, request):
		try:
			teacher = Teacher.objects.get(id=int(request.data.get('teacher', 0)))
		except Teacher.DoesNotExist:
			teacher = None
		try:
			course = Course.objects.create(
				name=request.data.get('name', None),
				code=request.data.get('code', None),
				teacher=teacher,
			)
			return JsonResponse(self.toDict(course), \
				status=status.HTTP_200_OK, safe=False)
		except:
			return JsonResponse({"status":"error"}, status=status.HTTP_400_BAD_REQUEST, safe=False)

	def put(self, request, pk):
		try:
			course = Course.objects.get(id=pk)
		except Course.DoesNotExist:
			return HttpResponse({"status":"error not found"}, content_type="json", \
				status=status.HTTP_404_NOT_FOUND)

		try:
			teacher = Teacher.objects.get(id=int(request.data.get('teacher', 0)))
		except Teacher.DoesNotExist:
			teacher = None

		try:	
			course.name = request.data.get('name', None)
			course.code = request.data.get('code', None)
			course.teacher = teacher
			course.save()
			return JsonResponse(self.toDict(course), \
				status=status.HTTP_200_OK, safe=False)
		except:
			return JsonResponse({"status":"error"}, status=status.HTTP_400_BAD_REQUEST, safe=False)

	def toDictList(self, query):
		courses = []
		for course in query:
			courses.append(self.toDict(course))
		return courses

	def toDict(self, course):
		teacher_id = course.teacher.id if course.teacher else 0;
		teacher_name = " ".join((course.teacher.name,course.teacher.last_name_1)) if course.teacher else '-';
		return {
			"pk": course.id, 
			"fields":{
				"name": course.name,
				"code": course.code,
				"teacher": teacher_id,
				"teacher_name": teacher_name
			}
		}


class QuizView(APIView):


	def get(self, request):
		operation = request.GET.get('operation', None)
		course = request.GET.get('course', None)
		student = request.GET.get('student', None)
		if operation and course:
			if operation == 'course_in':
				quizzes = Quiz.objects.filter(course=course)
			elif operation == 'course_out':
				quizzes = Quiz.objects.exclude(course=course)
			elif operation == 'course_in_student_out' and student:
				try:
					student_course = StudentCourse.objects.get(course=course, student=student)
					quiz_with_score = list(QuizCourseStudent.objects.filter(student_course=student_course).values_list('quiz', flat=True))
					quizzes = Quiz.objects.filter(course=course).exclude(id__in=quiz_with_score)
				except StudentCourse.DoesNotExist:
					return JsonResponse({"status":"error not found"}, status=status.HTTP_404_NOT_FOUND \
						, safe=False)
			
			return HttpResponse(json.dumps(self.toDictList(quizzes)), content_type="json", status=status.HTTP_200_OK)
		else:
			return HttpResponse(json.dumps(self.toDictList(Quiz.objects.all())), content_type="json", status=status.HTTP_200_OK)


	def delete(self, request, pk):
		try:
			quiz = Quiz.objects.get(id=pk)
			quiz.delete()
			return JsonResponse({"status":"ok"}, status=status.HTTP_200_OK, safe=False)
		except Quiz.DoesNotExist:
			return JsonResponse({"status":"error not found"}, status=status.HTTP_404_NOT_FOUND \
				, safe=False)

	def post(self, request):
		try:
			course = Course.objects.get(id=int(request.data.get('course', 0)))
		except:
			return JsonResponse({"status":"error"}, status=status.HTTP_400_BAD_REQUEST, safe=False)		

		try:
			quiz = Quiz.objects.create(
				name=request.data.get('name', None),
				max_score=request.data.get('max_score', 10),
				approve_score=request.data.get('approve_score', 5),
				course=course
			)
			return JsonResponse(self.toDict(quiz), \
				status=status.HTTP_200_OK, safe=False)
		except:
			return JsonResponse({"status":"error"}, status=status.HTTP_400_BAD_REQUEST, safe=False)

	def put(self, request, pk):
		try:
			quiz = Quiz.objects.get(id=pk)
		except Quiz.DoesNotExist:
			return HttpResponse({"status":"error not found"}, content_type="json", \
				status=status.HTTP_404_NOT_FOUND)

		try:
			course = Course.objects.get(id=int(request.data.get('course', 0)))
		except Teacher.DoesNotExist:
			course = None

		try:	
			quiz.name = request.data.get('name', None)
			quiz.max_score = request.data.get('max_score', None)
			quiz.approve_score = request.data.get('approve_score', None)
			quiz.course=course
			quiz.save()
			return JsonResponse(self.toDict(quiz), status=status.HTTP_200_OK, safe=False)
		except:
			return JsonResponse({"status":"error"}, status=status.HTTP_400_BAD_REQUEST, safe=False)

	def toDictList(self, query):
		quizzes = []
		for quiz in query:
			quizzes.append(self.toDict(quiz))
		return quizzes

	def toDict(self, quiz):
		course_id = quiz.course.id if quiz.course else 0
		course_name = "-".join((quiz.course.name,quiz.course.code)) if quiz.course else '-'
		return {
			"pk": quiz.id, 
			"fields":{
				"course": course_id,
				"course_name": course_name,
				"name": quiz.name,
				"max_score": quiz.max_score,
				"approve_score": quiz.approve_score
			}
		}

class StudentCourseView(APIView):

	def post(self, request):
		try:
			student = Student.objects.get(id=int(request.data.get('student', 0)))
			course = Course.objects.get(id=int(request.data.get('course', 0)))
		except:
			return JsonResponse({"status":"error"}, status=status.HTTP_400_BAD_REQUEST, safe=False)		

		try:
			student_course = StudentCourse.objects.create(
				student=student,
				course=course
			)
			return JsonResponse(self.toDict(student_course), \
				status=status.HTTP_200_OK, safe=False)
		except:
			return JsonResponse({"status":"error"}, status=status.HTTP_400_BAD_REQUEST, safe=False)

	def delete(self, request, student_pk, course_pk):
		try:
			student = Student.objects.get(id=student_pk)
			course = Course.objects.get(id=course_pk)
		except:
			return JsonResponse({"status":"error not found"}, status=status.HTTP_404_NOT_FOUND \
				, safe=False)

		student_course = StudentCourse.objects.get(student=student, course=course)
		student_course.delete()
		return JsonResponse({"status":"ok"}, status=status.HTTP_200_OK, safe=False)

	def toDictList(self, query):
		students_courses = []
		for student_course in query:
			students_courses.append(self.toDict(student_course))
		return students_courses

	def toDict(self, student_course):
		return {
			"pk": student_course.id, 
			"fields":{
				"course": student_course.course.id,
				"course_name": student_course.course.name,
				"student": student_course.student.id,
				"student_name": " ".join((student_course.student.name, student_course.student.last_name_1)),
				"student_rut": student_course.student.identity_card_number
			}
		}


class ScoreView(APIView):

	def get(self, request):
		scores = self.toDictList(QuizCourseStudent.objects.all())
		return HttpResponse(json.dumps(scores), content_type="json", status=status.HTTP_200_OK)		

	def post(self, request):
		try:
			student = int(request.data.get('student', 0))
			course = int(request.data.get('course', 0))
			student_course = StudentCourse.objects.get(student=student, course=course)
			quiz = Quiz.objects.get(id=int(request.data.get('quiz', 0)))
		except:
			return JsonResponse({"status":"error"}, status=status.HTTP_400_BAD_REQUEST, safe=False)		

		try:
			score = QuizCourseStudent.objects.create(
				student_course=student_course,
				quiz=quiz,
				score=request.data.get('number', 0)
			)
			return JsonResponse(self.toDict(score), \
				status=status.HTTP_200_OK, safe=False)
		except:
			return JsonResponse({"status":"error not found"}, status=status.HTTP_404_NOT_FOUND \
				, safe=False)

	def put(self, request, pk):
		try:
			score = QuizCourseStudent.objects.get(id=pk)
		except QuizCourseStudent.DoesNotExist:
			return HttpResponse({"status":"error not found"}, content_type="json", \
				status=status.HTTP_404_NOT_FOUND)

		try:	
			score.score = request.data.get('number', 0) if int(request.data.get('number', 0)) <= 10 else 10
			score.save()
			return JsonResponse(self.toDict(score), status=status.HTTP_200_OK, safe=False)
		except:
			return JsonResponse({"status":"error"}, status=status.HTTP_400_BAD_REQUEST, safe=False)


	def delete(self, request, pk):
		score = QuizCourseStudent.objects.get(id=pk)
		score.delete()
		return JsonResponse({"status":"ok"}, status=status.HTTP_200_OK, safe=False)

	def toDictList(self, query):
		scores = []
		for score in query:
			scores.append(self.toDict(score))
		return scores

	def toDict(self, score):
		return {
			"pk": score.id, 
			"fields":{
				"course": score.student_course.course.id,
				"course_name": score.student_course.course.name,
				"student": score.student_course.student.id,
				"student_name": " ".join((score.student_course.student.name, score.student_course.student.last_name_1)),
				"quiz": score.quiz.id,
				"quiz_name": score.quiz.name,
				"score":int(score.score),
				"student_course": score.student_course.id
			}
		}


class StudentsAvg(APIView):
	
	def get(self, request):
		operation = request.GET.get('operation', 'avg')

		if operation == 'avg':		
			students = Student.objects.all()
			students_dict = []

			for student in students.iterator():
				students_dict.append(self.studentToDict(student))

			return HttpResponse(json.dumps(students_dict), content_type="json", status=status.HTTP_200_OK)
		elif operation == 'avg_by_reproved':
			reproved_courses = list(QuizCourseStudent.objects.all().values('student_course')
				.annotate(avg_by_course=Avg('score')).filter(avg_by_course__lt=5)
				.values_list('student_course_id', flat=True))

			reproved_students_ids = list(StudentCourse.objects.filter(id__in=reproved_courses) \
				.values_list('student', flat=True).distinct())
			
			students = Student.objects.filter(id__in=reproved_students_ids)
			students_dict = []

			for student in students.iterator():
				students_dict.append(self.studentToDict(student))

			return HttpResponse(json.dumps(students_dict), content_type="json", status=status.HTTP_200_OK)
	
	def studentToDict(self, student):
		return {
			"pk": student.id, 
			"fields":{
				"name": " ".join((student.name, student.last_name_1)),
				"rut": student.identity_card_number,
				"avg": student.calutaleAvg(),
			}
		}

			
