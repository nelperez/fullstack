# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db.models import Avg

class Teacher(models.Model):
    u"""Represetnacion de los profesores adscritos a la plataforma.

    Attributes
    ----------
    name : CharField
        Nombres del profesor.
    last_name_1: CharField
        Primer apellido o apellido paterno.
    last_name_2 : CharField
        Segundo apellido o apellido materno.
    created : DateTimeField
        Fecha y hora de creación del profesor.

    :Authors:
        - Nel Pérez

    :Last Modification:
        - 11.03.2018        
    """

    name = models.CharField(max_length=80, null=False, blank=False)
    last_name_1 = models.CharField(max_length=40, null=False, blank=False)
    last_name_2 = models.CharField(max_length=40, default='')
    created = models.DateTimeField(auto_now_add=True, editable=False)

    def __unicode__(self):
        return " ".join((self.name, self.last_name_1))

    class Meta:
        verbose_name = 'Teacher'
        verbose_name_plural = 'Teachers'
        app_label = 'university'


class Student(models.Model):
    u"""Represetnacion de los estudiantes adscritos a la plataforma.

    Attributes
    ----------
    name : CharField
        Nombres del alumno.
    last_name_1: CharField
        Primer apellido o apellido paterno del alumno.
    last_name_2 : CharField
        Segundo apellido o apellido materno del alumno.
	identity_card_number : CharField
		Numero identificador unico personal (en caso de Chile RUT)
    created : DateTimeField
        Fecha y hora de registro del alumno.

    :Authors:
        - Nel Pérez

    :Last Modification:
        - 11.03.2018        
    """

    name = models.CharField(max_length=80, null=False, blank=False)
    last_name_1 = models.CharField(max_length=40, null=False, blank=False)
    last_name_2 = models.CharField(max_length=40, default='')
    identity_card_number = models.CharField(max_length=9, null=False, blank=False)
    created = models.DateTimeField(auto_now_add=True, editable=False)

    def __unicode__(self):
        return " ".join((self.name, self.last_name_1))

    class Meta:
        verbose_name = 'Student'
        verbose_name_plural = 'Students'
        app_label = 'university'

    def calutaleAvg(self):
        avg = QuizCourseStudent.objects.filter(student_course__student=self).aggregate(avg=Avg('score'))
        return avg['avg'] if avg['avg'] else '-'

    def getStudentCourses(self):
         return StudentCourse.objects.filter(student=self)        


class Course(models.Model):
    u"""Represetnacion de los cursos o materias disponibles en la plataforma.
	
	Considerations
	--------------
	* Un curso puede crearse sin que se asigne un profesor, el mismo puede ser
	asignado luego.

    Attributes
    ----------
    name : CharField
        Nombre del curso o materia.
    code: CharField
        Codigo asignado a la materia o curso con el fin de identificarlo mas
        facilmente.
    teacher  : Teacher
    	Profesor asignado al curso, el mismo puede ser fijado luego de la creacion
    	del curso.
    created : DateTimeField
        Fecha y hora de registro del curso.

    :Authors:
        - Nel Pérez

    :Last Modification:
        - 11.03.2018        
    """	
    name = models.CharField(max_length=200, null=False, blank=False)
    code = models.CharField(max_length=10, null=False, blank=False)
    teacher = models.ForeignKey(Teacher, null=True, blank=True, \
    	on_delete=models.SET_NULL)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = 'Course'
        verbose_name_plural = 'Courses'
        app_label = 'university'


class Quiz(models.Model):
    u"""Pruebas o examanes tomados por los alumnos en cada curso.

    Attributes
    ----------
    name : CharField
        Nombre del examen.
    created : DateTimeField
        Fecha y hora de registro de la prueba.

    :Authors:
        - Nel Pérez

    :Last Modification:
        - 11.03.2018        
    """
    name = models.CharField(max_length=200, null=False, blank=False)
    max_score = models.IntegerField(blank=False, null=False, default=10)
    approve_score = models.IntegerField(blank=False, null=False, default=5)
    course = models.ForeignKey(Course, on_delete=models.SET_NULL, null=True, \
        blank=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = 'Quiz'
        verbose_name_plural = 'Quizzes'
        app_label = 'university'


class StudentCourse(models.Model):
    u"""Represetnacion de los cursos tomados por los alumnos.
	
	Considerations
	--------------
	* Un estudiante solo puede estar asociado una vez a un curso.

    Attributes
    ----------
    student : Student
        Clave foranea a la entidad estudiante.
    course: Course
        Clave foranea a la entidad curso.
    created : DateTimeField
        Fecha y hora de registro del alumno en el curso.

    :Authors:
        - Nel Pérez

    :Last Modification:
        - 11.03.2018        
    """
    student =  models.ForeignKey(Student, on_delete=models.CASCADE, null=False, \
		blank=False)
    course = models.ForeignKey(Course, on_delete=models.CASCADE, null=False, \
		blank=False)
    created = models.DateTimeField(auto_now_add=True, editable=False)	

    def __unicode__(self):
        return "-".join((self.course.code,self.student.identity_card_number))

    class Meta:
        verbose_name = 'StudentCourse'
        verbose_name_plural = 'StudentsCourses'
        app_label = 'university'
        unique_together = ('course', 'student')


class QuizCourseStudent(models.Model):
    u"""Represetnacion de las notas asignadas a las pruebas hechas por los
    estudiantes en cada curso.
	
	Considerations
	--------------
	* Un estudiante solo puede contar con una nota por cada prueba en cada curso.
	* Las notas dentro de las pruebas comprenden desde el 0 hasta el 10 inclusive.
	* Las notas se almacenaran con tan solo un decimal.
	* Se contara como reprobada, cualuqier nota por debajo o igual a cinco.

    Attributes
    ----------
    score : CharField
        Nota conseguida por el alumno en la prueba asociada.
    student_course: StudentCourse
        Combinacion curso, estudiante.
    quiz : Quiz
        Prueba a la que se le asocia la nota conseguida por el estudiante.

    :Authors:
        - Nel Pérez

    :Last Modification:
        - 11.03.2018        
    """
    score = models.DecimalField(validators=[MinValueValidator(0), MaxValueValidator(10)], \
		decimal_places=1, max_digits=3, null=False, blank=False)
    student_course =  models.ForeignKey(StudentCourse, on_delete=models.CASCADE, null=False, \
		blank=False)
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE, null=False, \
		blank=False)	

    def __unicode__(self):
        return "-".join((self.course.code, str(self.score)))

    class Meta:
        verbose_name = 'QuizCourseStudent'
        verbose_name_plural = 'QuizzesCoursesStudents'
        app_label = 'university'
        unique_together = ('student_course', 'quiz')