import Vue from 'vue'
import VueResource from 'vue-resource'
import teacher from './components/teacher.vue'
import student from './components/student.vue'
import course from './components/course.vue'
import quiz from './components/quiz.vue'
import score from './components/score.vue'
import lodash from 'lodash'

Vue.use(VueResource, lodash)

new Vue(teacher).$mount("#teacher-container")
new Vue(student).$mount("#student-container")
new Vue(course).$mount("#course-container")
new Vue(quiz).$mount("#quiz-container")
new Vue(score).$mount("#score-container")