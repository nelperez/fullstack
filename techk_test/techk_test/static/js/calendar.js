function GuideLyne($container, time_config) {

	var self = this;
	this.wrapper = null;
	this.blocks = [];
	var $templateContainer = $("<div class='guideline-container'></div>");
	var $header = $("<div class='header'>Horas</div>");
	
	function init() {
		self.wrapper = $templateContainer.appendTo($container);
		$header.appendTo(self.wrapper);
		for (i = 0; i <= time_config.total_row; i++) { 
		    addBlock(i);
		}
	}

	function addBlock(i) {
		if(self.wrapper) {
			var block = new Block(i);
			block.$elem = block.template.appendTo(self.wrapper);
			self.blocks.push(block);
		}
	}

	function Block(i) {
		var self = this;
		var title = null;
		this.$elem = null;
		this.template = $("<div class='block'><h1></h1></div>");
		
		function setTitle(){
			var rel = (i * (60 / time_config.hour_division))/60;
			var first_poart = Math.floor(rel);
			var second_poart = rel - first_poart;
			var hour = Math.floor(rel) + time_config.start_time;
			var min = (second_poart * 60) == 0 ? "00":Math.round(second_poart * 60);
			title = hour+":"+min;

			self.template.find("h1").html(title);
		}

		function init(){
			setTitle();
		}

		init();
	}

	init();
}

function WeekManager($calendar_container, time_config, labels) {

	var self = this;
	this.days = [];
	this.$el = null
	this.$templateContainer = $("<div class='week-container'></div>");
	

	function init(){
		self.$el = self.$templateContainer.appendTo($calendar_container);
		

		for (i = 0; i < 5; i++) { 
		    addDay(labels[i]);
		}		
	}

	function addDay(title) {
		var day = new DayManager(self.$el, title, time_config);
		self.days.push(day);
	}

	this.processData = function(data){
		$.each(data, function(day_label, day_data) {
			console.log(day_label);
		    self.days[labels.indexOf(day_label)].processData(day_data);		  
		});
	}

	init();
}

function DayManager($container, title, time_config) {
	
	var self = this;
	this.$el = null;
	this.blocks = []
	var $templateContainer = $("<div class='day-container'></div>");
	var $header = $("<div class='header'>"+title+"</div>");

	function init(){
		self.$el = $templateContainer.appendTo($container);
		$header.appendTo(self.$el);

		for (var i = 0; i <= time_config.total_row; i++) { 
		    addBlock(i);
		}
	}

	function addBlock(i){
		var blockDay = new BlockDay(self.$el, i, time_config);
		self.blocks.push(blockDay);
	}

	this.processData = function(data){
		$.each(data, function(index, day_data) {
		    var start_time = day_data.start_time.split(":");
		    var start_hour = parseInt(start_time[0]);
		    var start_min = parseInt(start_time[1]);

		    var end_time = day_data.end_time.split(":");
		    var end_hour = parseInt(end_time[0]);
		    var end_min = parseInt(end_time[1]);

			$.each(self.blocks, function(index, block) {
				console.log(block);
				if((block.start_hour > start_hour || (block.start_hour == start_hour && block.start_min >= start_min)) && 
					(block.end_hour < end_hour || (block.end_hour == end_hour && block.end_min <= end_min))) {
						$("<div class='calendar-item'>"+day_data.name+"</div>").appendTo(block.$el);
				}
				});
		});		
	}

	init();

}

function BlockDay($container, i, time_config) {
	var self = this;
	this.$el = null;
	var $template = $("<div class='block'></div>");
	this.start_hour = null;
	this.start_min = null;		
	this.end_hour = null;
	this.end_min = null;		

	function init(){
		var rel = (i * (60 / time_config.hour_division))/60;
		var first_poart = Math.floor(rel);
		var second_poart = rel - first_poart;
		self.start_hour = Math.floor(rel) + time_config.start_time;
		self.start_min = Math.round(second_poart * 60);
		self.$el = $template.appendTo($container);
		setEndTime()
	}

	function setEndTime(){
		var start_step = self.start_min + (60 / time_config.hour_division);
		if (start_step == 60) {
			self.end_min = 0;
			self.end_hour = self.start_hour+1;
		}else if (start_step > 60) {
			self.end_min = 60 - start_step;
			self.end_hour = self.start_hour+1;
		}else {
			self.end_min = start_step;
			self.end_hour = self.start_hour;
		}
	}

	init();
}

function Calendar(time_config, data){

	var self = this;
	var labels = ["monday","tuesday","wednesday","thursday","friday"];
	time_config.total_row = (time_config.end_time - time_config.start_time) * time_config.hour_division

	this.$container = $("#calendar-container");
	this.guidelyne = new GuideLyne(self.$container, time_config);
	this.weekManager = new WeekManager(self.$container, time_config, labels);

	self.weekManager.processData(data);
}

data = {
    "monday": [
        {"name": "Jorge", "start_time": "08:00", "end_time": "09:00"},
        {"name": "Afonso", "start_time": "09:30", "end_time": "11:00"},
        {"name": "Sebastian", "start_time": "15:00", "end_time": "16:00"},
        {"name": "Augusto", "start_time": "17:00", "end_time": "19:30"}
    ],
    "tuesday": [
        {"name": "Jorge", "start_time": "08:00", "end_time": "09:00"},
        {"name": "Jorge", "start_time": "11:30", "end_time": "12:00"},
        {"name": "Jorge", "start_time": "15:00", "end_time": "16:00"},
        {"name": "Jorge", "start_time": "17:00", "end_time": "19:30"}
    ],
    "wednesday": [
        {"name": "Jorge", "start_time": "08:00", "end_time": "09:00"},
        {"name": "Jorge", "start_time": "10:30", "end_time": "12:00"},
        {"name": "Jorge", "start_time": "15:00", "end_time": "16:00"},
        {"name": "Jorge", "start_time": "17:00", "end_time": "19:30"}
    ],
    "thursday": [
        {"name": "Jorge", "start_time": "08:00", "end_time": "09:00"},
        {"name": "Jorge", "start_time": "09:30", "end_time": "12:00"},
        {"name": "Jorge", "start_time": "15:00", "end_time": "16:00"},
        {"name": "Jorge", "start_time": "17:00", "end_time": "19:30"}
    ],
    "friday": [
        {"name": "Jorge", "start_time": "08:00", "end_time": "09:00"},
        {"name": "Jorge", "start_time": "09:30", "end_time": "12:00"},
        {"name": "Jorge", "start_time": "15:00", "end_time": "16:00"},
        {"name": "Jorge", "start_time": "17:00", "end_time": "19:30"}
    ]
}
var calendar = new Calendar({"start_time":8, "end_time":20, "hour_division":2}, data);