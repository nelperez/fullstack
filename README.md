## Supuestos para el proyecto:

* Las notas las considero desde 0 a 7 con decimales
* Como se habla de curso y ramo los considero lo mismo
* Considero nota roja si es menor a 4 y si tiene mas de un curso rojo esta reprobando
* Si no tiene promedio, no se considera aun en los que están reprobando
* Los cursos pueden ser registrados sin un profesor y posteriormente ser asignado quien este a cargo
* Un estudiante solo puede estar registrado una vez en un curso
* Un estudiante no debe tener 2 notas para el mismo test en el mismo curso

## Respuestas a las preguntas SQL:

1. Escriba una Query que entregue la lista de  alumnos para el curso
"programacion"

```
SELECT
    s.id, s.name
FROM
    Student AS s
WHERE
    s.id
IN (
    SELECT
        sc.student_id
    FROM
        StudentCourse AS sc
    INNER JOIN
        Course AS c ON sc.course_id = c.id
    WHERE
        c.name = "programacion"
)
```

2. Escriba una Query  que calcule el promedio de notas de un alumno en un
curso.

```
SELECT
    s.id, s.name, s.rut, AVG(tsc.score)
FROM
    student AS s
INNER JOIN
    student_course AS sc ON s.id = sc.student_id
INNER JOIN
    quiz_student_sourse AS qsc ON sc.id = qsc.student_course_id
WHERE
    sc.course_id = "course_id" AND s.id = "student_id"
```

3. Escriba una Query que entregue a los alumnos y el promedio que tiene
en cada curso.

```
SELECT
    c.id, c.name, s.id, s.name, s.last_name_1, s.rut, AVG(qsc.score)
FROM
    course AS c
INNER JOIN
    student_course AS sc ON c.id = sc.course_id
INNER JOIN
    student AS s ON s.id = sc.student_id
INNER JOIN
    quiz_student_course AS qsc ON sc.id = qsc.student_course_id
GROuP BY c.id, c.name, s.id, s.name
```

4. Escriba una Query que lista a todos los alumnos con mas de un curso con
promedio rojo.

```
SELECT students.student_id, s.name
from (
    SELECT
        sc.student_id, AVG(tsc.score)
    FROM
        student_course AS sc
    INNER JOIN
        quiz_student_course AS qsc ON sc.id = qsc.student_course_id
    GROUP BY
        sc.id
    HAVING AVG(tsc.score) <= 5
) AS students
INNER JOIN
    student AS s ON s.id = students.student_id
GROUP BY
    students.student_id
HAVING COUNT(students.student_id) > 1
```

5. Dejando de lado el problema del colegió se tiene una tabla con información de jugadores de tenis:
`PLAYERS(Nombre, Pais, Ranking)`. Suponga que Ranking es un numero de 1 a
100 que es distinto para cada jugador. Si la tabla en un momento dado
tiene
solo 20 registros, indique cuantos registros tiene la tabla que resulta de la
siguiente consulta:

```
SELECT c1.Nombre, c2.Nombre
FROM PLAYERS c1, PLAYERS c2
WHERE c1.Ranking > c2.Ranking
```
Seleccione las respuestas correctas:

```
a) 400
*b) 190*
c) 20
d) imposible saberlo
```
> La Respuesta es 190 solo hay que hacer la sumatoria regresiva de los numeros 19 a 1.

### Desarrollo

> Se utilizo tal como se soliticito Django, agregando a Vue como controlador basico del Flujo, todo ello en un venv con los requerimientos establecidos

Corriendo el proyecto
```
pip install -r requirements.txt
python manage.py runserver
```

> Todos los Cruds se encuentran en 5 secciones que se visualizan en el home, en la seccion de alumnos se verifican los promedios y reprobados, asi como en la seccion de cursos se incriben a los estudiantes


